#!/usr/bin/env ruby

require './encryption-oracle.rb'

class TrueClass; def to_i; 1; end end
class FalseClass; def to_i; 0; end end

class EncryptionDetector
  def initialize(oracle)
    @oracle = oracle
  end

  def perform_tests(n)
    done = 0.upto(n - 1).inject(0) do |accumulator, i|
      accumulator + oracle.last_mode_is(detect_mode.to_i).to_i
    end
    printf("Succeeded in %d/%d", done, n)
  end

  # returns true if ECB
  def detect_mode()
    blocks = @oracle.encrypt('a'*256).scan(/[\s\S]{16,16}/)[1..-2]
    blocks[0] == blocks[1]
  end
end


