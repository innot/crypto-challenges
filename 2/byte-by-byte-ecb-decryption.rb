#!/usr/bin/env ruby

require './encryption-oracle.rb'
require './ecb-cbc-detection-oracle.rb'

# doesn't decrypt unicode strings yet
class EcbDecryptor
  def initialize(oracle)
    raise 'an oracle wanted' unless oracle.is_a?(EcbOracle)
    @oracle = oracle
    #@blocksize = nil
    #@message_length = nil
    @message = ''
  end

  def get_sizes
    init_cipher_size = @oracle.encrypt('').size
    padding = 'x'
    while true do
      new_cipher = @oracle.encrypt(padding)
      if new_cipher.size > init_cipher_size then
        @blocksize = new_cipher.size - init_cipher_size
        @message_length = new_cipher.size - padding.size - @blocksize
        break
      end
      padding += 'x'
    end
    @padding_size = (@message_length/@blocksize + 1)*@blocksize
  end

  def ensure_ecb
    detector = EncryptionDetector.new(@oracle)
    raise 'wrong cipher mode' unless detector.detect_mode
    @ecb_mode = true
  end

  def decipher_message
    raise 'get sizes first before calling get_message' unless @padding_size != nil
    raise 'ensure that ecb is used before calling get_message' unless @ecb_mode != nil
    padding = 'x'*@padding_size
    block_i = (padding.size - 1)/@blocksize
    @message_length.times do
      padding = padding[1..-1]
      target = EncryptionOracle.get_blocks(@oracle.encrypt(padding), @blocksize)[block_i]
      printf("%d: block_i is %d\n", padding.size, block_i)
      0.upto(128) do |i|
        raise 'bad decription' if i == 128
        byte = [i].pack('C')
        attempt = EncryptionOracle.get_blocks(@oracle.encrypt(padding + @message + byte), @blocksize)[block_i]
        if attempt == target then
          @message += byte
          break
        end
      end
    end
  end

  attr_reader :message_length, :blocksize, :message
end
