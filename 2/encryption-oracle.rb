#!/usr/bin/env ruby

require './aes-cbc.rb'

class EncryptionOracle
  def initialize
    @last_mode = nil
  end

  def self.random_string(size)
    0.upto(size - 1).map{ |i| rand(128) }.pack('C*')
  end

  def self.get_blocks(string, blocksize)
    string.scan(/[\s\S]{#{blocksize},#{blocksize}}/)
  end

  def encrypt(data)
    data = self.random_string(5 + rand(6)) + data + self.random_string(5 + rand(6))
    @last_mode = rand(2)
    if @last_mode == 1
      AES_ECB::encrypt_aes_ecb_128(data, self.ran($cipher_len))
    else
      AES_CBC::encrypt_aes_cbc_128(data, self.random_string($cipher_len), self.random_string($cipher_len))
    end
  end

  def last_mode_is(mode)
    return @last_mode == mode
  end
end

class EcbOracle
  def initialize(message)
    raise 'string wanted as a message' unless message.is_a?(String)
    @message = message
    @requests = 0
    @key = EncryptionOracle.random_string($cipher_len)
  end

  def encrypt(data)
    @requests += 1
    AES_ECB::encrypt_aes_ecb_128(data + @message, @key)
  end

  attr_reader :requests
end
