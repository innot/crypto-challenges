#!/usr/bin/env ruby

require 'set'

def count_equal_blocks(str, blocksize)
  blocks = str.scan(/.{#{blocksize},#{blocksize}}/)
  blocks.size - blocks.uniq.size
end

def get_enc_string(file, blocksize)
  File.open(file).readlines.map do |line|
    [count_equal_blocks(line, blocksize), line]
  end.sort do |x,y|
    x[0] <=> y[0]
  end.each do |i|
    print i[0].to_s + ' -> ' + i[1]
  end
end

get_enc_string(ARGV[0], 16)
