#!/usr/bin/env ruby

def hex_to_base64(hex)
  [[hex].pack('H*')].pack('m').strip
end

print hex_to_base64(ARGV[0])

