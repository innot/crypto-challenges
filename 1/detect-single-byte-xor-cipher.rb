#!/usr/bin/env ruby

def hex_to_intarray(s)
  s.scan(/../).map{ |s| s.hex }
end

def fixed_xor(s0, s1)
  raise 'two strings please' unless (s0.is_a?(String)) and (s1.is_a?(String))
  xor = hex_to_intarray(s0.ljust(60, '0')).zip(hex_to_intarray(s1)).map{ |x,y| x^y }.map{ |s| s.to_s(16).ljust(2, '0') }.join
end

def decrypt(cipher, key)
  extended_key = key*cipher.size
  fixed_xor(cipher, extended_key.unpack('H*')[0])
end

def all_variants(cipher)
  result = []
  raise 'hex-encoded string please' unless cipher.is_a?(String)
  'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'.chars.each do |key|
    result << [[decrypt(cipher, key)].pack('H*'), key]
  end
  return result
end

def metric(text)
  text.chars.inject(0) do |result,c|
    if c[/[a-z ,.:'"]/] == c
      result + 5
    elsif c[/[A-Z0-9!()+-@?*]/] == c
      result + 4
    elsif c[/&\^%\$#_=;/] == c
      result - 3
    else
      result - 10
    end
  end
end

def best_variant(variants)
  variants.map!{ |x| { i: metric(x[0]), text: x[0], key: x[1] } }.sort!{ |x,y| y[:i] <=> x[:i] }[0]
end

def find_cipher(file)
  hashes = []
  File.open(file).each{ |line| hashes << best_variant(all_variants(line)) }
  hashes.sort!{ |x,y| x[:i] <=> y[:i] }
end

find_cipher(ARGV[0]).each{ |i| p i }

