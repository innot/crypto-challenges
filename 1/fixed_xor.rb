#!/usr/bin/env ruby

def hex_to_intarray(s)
  s.scan(/../).map{ |s| s.hex }
end

def fixed_xor(s0, s1)
  raise 'two strings please' unless (s0 != nil) and (s1 != nil)
  hex_to_intarray(s0).zip(hex_to_intarray(s1)).map{ |x,y| x^y }.map{ |s| s.to_s(16) }.join
end

print fixed_xor(ARGV[0], ARGV[1])

